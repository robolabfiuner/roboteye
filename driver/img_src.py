# -*- coding: utf-8 -*-
""" driver es la capa de abstracción para el hardware de adquisición de imágenes.

EL modulo driver contiene la capa de abstracción de los objetos dedicados a la
iniciación y manipulación de el hardware para adquisición de imágenes.
"""
import cv2
import numpy as np
from PyQt5.QtCore import (pyqtSignal,
                          QThread)


class CamDriver(QThread):
    """CamDriver es una implementación en opencv para el uso de la webcam.

    CanDrive hereda de QThread de modo que el se la aplicación dedique un thread
    independiente a la adquisición de imágenes.
    """
    frames_src = pyqtSignal(np.ndarray)

    def __init__(self, device_id=0, parent=None):
        """__init__ Inicialización de CamDriver.

        Se establece el puerto de la dispositivo de adquisición de imágenes y se
        obtienen los parámetros de configuración del hardware.

        Keyword Arguments:
            device_id {int} -- Puerto del dispositivo de adquisición de imágenes. (default: {0})
            parent {QtObject} -- Parámetro heredado de QThread (default: {None})
        """
        QThread.__init__(self, parent)
        self.videocap = cv2.VideoCapture(device_id)
        self.videocap.set(cv2.CAP_PROP_FPS, 30.0)
        self.img_width = self.videocap.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.img_heigth = self.videocap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        self.stop_flag = False

    def stop(self):
        """stop Función que vuelve verdadero el atributo stop_flag."""
        self.stop_flag = True

    def __del__(self):
        """__del__Destructor del objeto."""
        # TODO: Analizar el destructor y las funciones utilizadas.
        self.quit()
        self.wait()
        # Se libera el dispositivo de captura.
        self.videocap.release()

    def run(self):
        """run Método que lanza la ejecución del Thread.

        Mientras el no se invoque el método stop, se adquieren y se
        emiten una a una.
        """
        while not self.stop_flag:
            ret, updateframe = self.videocap.read()
            if ret:
                self.frames_src.emit(updateframe)
        else:
            self.stop_flag = False
