# RobotEye FIUNER-LAFEDAR

Diseño y desarrollo de un sistema de reconocimiento de posición y orientación de
blisters de comprimidos para celda robotizada de empaquetamiento en Laboratorios
LAFEDAR de la ciudad de Paraná, Entre Ríos.

- [RobotEye FIUNER-LAFEDAR](#roboteye-fiuner-lafedar)
  - [Equipo](#equipo)
    - [Organizaciones](#organizaciones)
    - [Resumen](#resumen)
    - [Ejemplo PyQt+OpenCV](#ejemplo-pyqtopencv)
      - [Objetivo](#objetivo)
      - [Descripción](#descripci%C3%B3n)
      - [Instalación](#instalaci%C3%B3n)
        - [Dependencias de Python](#dependencias-de-python)
      - [Ejecución](#ejecuci%C3%B3n)
      - [Árbol de directorios y Modelo Modular](#%C3%A1rbol-de-directorios-y-modelo-modular)
        - [Diagrama en bloques](#diagrama-en-bloques)

## Equipo

### Organizaciones
 **Facultad de ingeniería de la Universidad Nacional de Entre Ríos**
[http://www.ingenieria.uner.edu.ar/](http://www.ingenieria.uner.edu.ar/)

 *Cuerpo Docente*
 
 * Gerardo Gabriel Gentiletti [ggentiletti@ingenieria.uner.edu.ar](ggentiletti@ingenieria.uner.edu.ar)
 * Rosa María Weisz [rweisz@ingenieria.uner.edu.ar ](rweisz@ingenieria.uner.edu.ar )
 * Juan Cruz Gassó Loncan [jcgassoloncan@ingenieria.uner.edu.ar](jcgassoloncan@ingenieria.uner.edu.ar)
 * Leandro Mayrata [lmayrata@ingenieria.uner.edu.ar](lmayrata@ingenieria.uner.edu.ar)
 
 *Cuerpo Alumno*
 * Corina Cordoba [cordobacorinam@gmail.com](cordobacorinam@gmail.com)
 * Duilio Deangeli [deangeli37@gmail.com](deangeli37@gmail.com)
 * Nahuel Richard [bionahuelr@gmail.com](bionahuelr@gmail.com)
 
 **Laboratorios LAFEDAR S.A.**
 [http://www.lafedar.com.ar/](http://www.lafedar.com.ar/)
    
 * Empresa Privada 
 * Contacto: Ariel Niz


### Resumen

El presente proyecto se propone la elaboración de un sistema de visión 
computacional que permita la detección, en tiempo real, de parámetros espaciales
de blisters de comprimidos que circulan sobre cinta transportadora en la Planta 
Farmacéutica Laboratorios LAFEDAR S. A. . Dichos parámetros son definidos por 
personal de la Planta, por lo que resulta indispensable el trabajo conjunto e 
interdisciplinario con la empresa. La información obtenida será transmitida a un
sistema robotizado para que realice con precisión las tareas de selección y 
empaquetado de blisters en envases primarios.

### Ejemplo PyQt+OpenCV

#### Objetivo

Este ejemplo fue pensado para establecer un punto de partida del desarrollo de software del corriente proyecto. Los principales aspectos a destacar con este ejemplo son la de un **desarrollo modular** de bloques relativamente independientes y la estructura de **árbol de carpetas** siguiendo esta lógica. Así mismo se busca aprovechar esta característica para lograr una ejecución *multithreading* de la aplicación.

#### Descripción

El ejemplo plantea el desarrollo de una pequeña aplicación con una interfaz de
usuario que permite observar las capturas de la webcam. La aplicación cuenta
con un botón de *Play* y uno de *Stop*, más un botón de selección que permite
activar o desactivar el modo a escala de grises. Además la aplicación imprime
por consola mensajes y un calculo aproximado de las FPS del video.

<div style="text-align: center"><img src="resources/img/app.png" width=800 alt="salida_app">

***Ejemplo de la salida de la aplicación**: A la izquierda se ve como se imprimen por consola un cálculo aproximado de las FPS y algunos mensajes. A la Derecha interfaz de la aplicación con la salida de video y los botones.*
</div>

#### Instalación

Tanto el ejemplo como a lo largo de todo el proyecto, estaremos usando **Python
3.6.x** o superior. Luego solo hace falta clonar el repositorio o descargar en
formato comprimido.

##### Dependencias de Python

* opencv_python >= 4.0.0.21
* numpy >= 1.16.2
* PyQt5 >= 5.12.1

**Instalación automática con pip**

```
git clone https://gitlab.com/robolabfiuner/roboteye.git
cd roboteye
pip install -r requirements.txt
```

#### Ejecución

Desde una terminal:
```
cd roboteye
python app.py
```


#### Árbol de directorios y Modelo Modular

El árbol de directorios intenta encapsular cada uno de los módulos de la
aplicación a partir de su función principal:

 * >`driver:`Este modulo intenta encapsular todos aquellos scripts relativos al
   > manejo del hardware de una cámara y configuraciones disponibles, por ejemplo
   > seteo de FPS, resolución, modo de color, etc.
   >> `img_src.py:`Este script contiene un simple ejemplo de como usar la webcam
   >> con opencv y el seteo de algunos parámetros. Además modela un ejemplo para que la
   >> adquisición de frames en un thread independiente. El script genera una
   >> _pyqtSignal_ con el frame actual para que pueda ser usado por otro módulo.
 * >`interface:`Este módulo encapsula los scripts relacionados con la interfaz
   > de usuario implementada en PyQt.
   >> `gui_handler.py:` Contiene la clase principal de _GUI_ a partir de la cual
   >> se manejan las diferentes señales emitidas por la interfaz y su
   >> consiguiente acción, a partir de los módulos `driver` y `machine`.
   >
   >> `GUI.py: ` Es el script de PyQt puro, autogenerado por el comando de
   >> consola `pyui5` a partir del archivo XML `GUI.ui` a mediante la
   >> herramienta _designer_ de Qt. El archivo `*.py` debe ser actualizado
   >> siempre que sea modificado el archivo `*.ui`.
 * >`machine:`Este módulo encapsula el motor de procesamiento de visión
   >computacional como comunicación de la aplicación.
   >> `processor.py:`En este caso script contiene un pequeño ejemplo para el
   >> calculo de las FPS, pero además la clase madre _Processor_ que se ejecuta en
   >> un hilo de proceso independiente, tomando las imágenes emitidas por el
   >> módulo `driver`, realiza el procesamiento pertinente para finalmente,
   >> convertir el frame en el formato compatible con Qt, para que el módulo
   >> `interface` los pueda usar.
 * > `resource:`Este directorio no representa un módulo de la aplicación como tal,
   > sino un directorio donde se almacenan los diferentes recursos o archivos no
   > ejecutables que forman parte de la aplicación.
   >> `GUI.ui:` Archivo XML a parir de la herramienta _designer_ de Qt.
   >
   >> `img/:` Directorio donde almacenar imágenes relacionadas.
 * > `app.py: ` Este script es el encargado de inicializar e instanciar los
   diferentes objetos de la aplicación, como archivo ejecutable de la aplicación.
 * > `requirements.txt: `Mantiene el listado de las dependencias de la aplicación.
   >> A partir de este archivo se pueden instalar las dependencias con la
   herramienta _pip_: `pip install -r requirements.txt`

##### Diagrama en bloques

<div style="text-align: center"><img src="resources/img/diagrama_ej.png" width=500 alt="salida_app">