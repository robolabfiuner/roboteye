# -*- coding: utf-8 -*-
""" El módulo machine encapsula el motor de procesamiento de visión computacional como comunicación de la aplicación."""
import datetime
import cv2
import numpy as np
import PyQt5.QtCore
from PyQt5.QtCore import (pyqtSignal,
                          pyqtSlot,
                          QThread)
from PyQt5.QtGui import (QImage,
                         QPixmap)


class FPS:
    def __init__(self):
        """__init__ Inicialización
        
        Almacena el tiempo de inicio, el tiempo de fin y el numero de frames contadas.
        """
        self._start = None
        self._end = None
        self._numFrames = 0

    def start(self):
        """start: Comienzo de conteo.
        
        Se almacena el tiempo de inicio.
        """
        self._start = datetime.datetime.now()
        return self

    def stop(self):
        """stop: Fin de conteo.
        
        Se almacena el tiempo de fin.
        """
        self._end = datetime.datetime.now()

    def update(self):
        """update: Contador de frames.
        
        Actualiza el contador de frames.
        """

        self._numFrames += 1

    def elapsed(self):
        """elapsed: Tiempo transcurrido.
        
        Calculo el tiempo total transcurrido en segundos.
        
        Returns:
            [float] -- Diferencia entre el fin y el inicio, forzado a segundos.
        """
        return (self._end - self._start).total_seconds()

    def fps(self):
        """fps: Frames por segundos.
        
        Calculo promedio de frames por segundos luego de un determinado tiempo transcurrido.
        
        Returns:
            [float] -- Promedio entre la cuenta de frames y el tiempo transcurrido.
        """
        return self._numFrames / self.elapsed()

    def reset(self):
        self._start = None
        self._end = None
        self._numFrames = 0
        self.start()


class Processor(QThread):
    """Processor Motor de procesamiento de imágenes independiente.

    Toma las imágenes curdas emitidas por el módulo driver, realiza el procesamiento pertinente para finalmente, convertir el frame en el formato compatible con Qt, para que el módulo interface los pueda usar.
    """
    #Señal para transmitir las imágenes procesadas.
    changePixmap = pyqtSignal(QPixmap)

    def __init__(self, src=None, parent=None):
        """__init__ Inicialización del procesador.
        
        El procesador necesita de una fuente de imágenes del tipo numpy para realizar el
        procesamiento deseado.
        
        Keyword Arguments:
            src {[Obj]} -- [Objeto fuente de imágenes crudas.] (default: {None})
            parent {QtObject} -- Parámetro heredado de QThread (default: {None})
        """
        QThread.__init__(self, parent)
        self.fps = FPS()
        self.src = src
        # Banderas para activar o desactivar funciones.
        self.lastFrame = None
        self.qtframe = None
        self.stopFlag = False
        self.grayScale = False

    def stop(self):
        """stop: Para el procesamiento."""
        print("Stop processing..")
        self.stopFlag = True

    def __del__(self):
        """__del__ Destructor de objeto."""
        # TODO: Analizar el destructor y las funciones utilizadas.
        self.quit()
        self.wait()

    def send_qtframe(self):
        """send_qtframe Emite el Frame procesado en formato Qt compatible.
        
        Se emite el frame final luego del procesamiento pertinente y compatible con qt.
        """
        self.changePixmap.emit(self.qtframe)

    def grayScale_mode(self):
        """grayScale_mode Cambio de a escala de grises.
        
        Si la bandera grayScale es verdadera se activa activa el modo en escala de grises, de lo
        contrario pasa a modo RGB.
        
        Returns:
            [numpy Array] -- Frame en el modo de color correspondiente.
        """

        if self.grayScale:
            return cv2.cvtColor(self.lastFrame, cv2.COLOR_BGR2GRAY)
        else:
            return cv2.cvtColor(self.lastFrame, cv2.COLOR_BGR2RGB)

    def convert2Qtformat(self, frame):
        frame_height = frame.shape[1]
        frame_width = frame.shape[0]
        if len(frame.shape) == 3:
            channels = frame.shape[2]
            image_format = QImage.Format_RGB888 if channels == 3 else QImage.Format_Grayscale8
        else:
            image_format = QImage.Format_Grayscale8

        qt_format = QImage(frame.data,
                                   frame_height,
                                   frame_width,
                                   image_format)
        qt_format = QPixmap.fromImage(qt_format)
        qtformat = qt_format.scaled(640, 480, PyQt5.QtCore.Qt.KeepAspectRatio)
        return qtformat

    @pyqtSlot(np.ndarray)
    def process_frame(self, frame :np.ndarray):
        """process_frame Pipeline de procesamiento de la imagen.
        
        Esta función recibe como parámetro un frame al cual le aplica sucesivamente el procesamiento necesarios.
        
        Arguments:
            frame {np.ndarray} -- Imagen a procesar.
        """
        if not self.stopFlag:
            self.lastFrame = frame
            self.fps.update()
            frame = self.grayScale_mode()
            self.qtframe = self.convert2Qtformat(frame)
            if self.fps._numFrames >= 60:
                    self.fps.stop()
                    print("FPS: {}".format(self.fps.fps()))
                    print("n_frames: {} ,time: {}".format(self.fps._numFrames, self.fps.elapsed()))
                    self.fps.reset()
            self.send_qtframe()
        else:
            self.fps.reset()
            pass

    def run(self):
        """run Método que lanza la ejecución del Thread.
        
        Mientras la bandera de parada no este activada, se mantiene realiza el procesamiento de las
        imágenes recibidas.
        """
        self.fps.start()
        if not self.stopFlag:
            self.src.frames_src.connect(self.process_frame)
        else:
            self.stopFlag = False




