# -*- coding: utf-8 -*-
""" El módulo interface encapsula los scripts relacionados con la interfaz de usuario implementada en PyQt."""
from PyQt5.QtWidgets import QMainWindow
from interface.GUI import Ui_MainWindow


class Gui(QMainWindow):
    """Gui es la clase principal de la interface de usuario a partir de la cual se manejan las diferentes
    señales emitidas por la interfaz y su consiguiente acción, a partir de los módulos driver y machine.
    """

    def __init__(self, img_src, process, parent=None):
        """__init__ Inicialización de Gui.
        
        El inició de Gui necesita recibir como parámetros, el objeto que fuente de imágenes y el
        objeto encargado del procesamiento de las mismas.
        
        Arguments:
            img_src {Obj} -- [Objeto fuente de imágenes.]
            process {Obj} -- [Objeto procesador de imágenes.]
        
        Keyword Arguments:
            parent {QtObject} -- Parámetro heredado de QThread (default: {None})
        """
        super(Gui, self).__init__(parent)
        self.ui = Ui_MainWindow() # Instancia de la ventana principal.
        self.ui.setupUi(self) # Inicialización.
        self.video_placeholder = self.ui.label # Etiqueta donde se mostraran las imágenes
        # TODO: Analizar si label es el mejor componente para es mostrar imágenes.
        # -----Acciones y eventos de usuario con su respectivo callback.-------
        self.ui.pushButton.clicked.connect(self.playButtom)
        self.ui.pushButton_2.clicked.connect(self.stopWebCam)
        self.ui.radioButton.toggled.connect(self.grayScale)
        self.img_src = img_src # Instancia de la fuente de imágenes.
        self.process = process # Instancia del procesador

    def playButtom(self):
        """playButtom Comienza la adquisición de imágenes.
        
        Conecta las imágenes adaptadas a Qt por el procesador al espacio para las imágenes.
        Cheque si se esta corriendo el nodo de adquisición y de no estarlo lo ejecuta.
        """

        self.process.changePixmap.connect(self.video_placeholder.setPixmap)
        if not self.img_src.isRunning():
            self.img_src.start()
            if not self.process.isRunning():
                self.process.start()
        else:
            print("Video Capture already running")

    def stopWebCam(self):
        """stopWebCam Para la adquisición de imágenes.
        
        Chequea si el nodo de imágenes esta corriendo y luego lo para.
        """

        self.process.stop()
        if self.img_src.isRunning():
            print("Stop Capturing")
            self.img_src.stop()

    def grayScale(self):
        """grayScale Cambia las imágenes a escala de grises.
        
        Modifica la bandera del objeto de procesamiento para activar el cambio de formato de color.
        """
        if self.ui.radioButton.isChecked():
             self.process.grayScale = True
        else:
             self.process.grayScale = False
