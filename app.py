""" Script de ejecución de la aplicación completa.

Este script es el encargado de inicializar e instanciar los diferentes objetos de la aplicación, como archivo ejecutable de la aplicación.
"""
import sys
from PyQt5.QtWidgets import QApplication
from interface.gui_handler import Gui
from driver.img_src import CamDriver
from machine.processor import Processor

class App(QApplication):
    """App Aplicación 
    """

    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        self.source = CamDriver(0)
        self.machine = Processor(src=self.source)
        self.main_gui = Gui(img_src=self.source, process=self.machine)
        self.main_gui.show()


if __name__ == '__main__':
    app = App(sys.argv)
    sys.exit(app.exec_())
